use log::info;

/// A stream that writes each line to stdout
pub struct LogStream {
    buffer: Vec<u8>,
    log_fn: fn(&str),
}

impl LogStream {
    pub fn new(log_fn: fn(&str)) -> LogStream {
        LogStream {
            buffer: Vec::new(),
            log_fn,
        }
    }
}

impl std::io::Write for LogStream {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        // Split buffer into lines
        let mut lines = buf.split(|&b| b == b'\n');

        // Append the first line to the internal buffer
        if let Some(line) = lines.next() {
            self.buffer.extend_from_slice(line);
        }

        // If there are more lines, write the buffer and each line, except the
        // last one to stdout, keep the last line in internal buffer.
        // If `buf` contains whole lines, it will end with a `\n` and the last
        // line will be empty.
        for line in lines {
            (self.log_fn)(String::from_utf8_lossy(&self.buffer).as_ref());
            self.buffer = line.to_vec();
        }

        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        info!("{}", String::from_utf8_lossy(&self.buffer));
        self.buffer = vec![];
        Ok(())
    }
}
