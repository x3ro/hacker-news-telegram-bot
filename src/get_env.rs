#[macro_export]
macro_rules! get_env {
    ($name:expr) => {{
        let name = $name;
        std::env::var(name)
            .map_err(|err| format!("Missing environment variable \"{}\": {}", name, err))
    }};
    ($name:expr => $default:expr) => {
        get_env!($name).unwrap_or($default.to_string())
    };
    ($name:expr, $type:ty) => {{
        let name = $name;
        get_env!(name)?
            .parse::<$type>()
            .map_err(|err| format!("Failed to parse environment variable \"{}\": {}", name, err))
    }};
    ($type:ty, $name:expr) => {
        get_env!($name, $type)
    };
    ($name:expr, $type:ty => $default:expr) => {{
        let name = $name;
        std::env::var(name)
            .map(|s| {
                s.parse::<$type>().map_err(|err| {
                    format!("Failed to parse environment variable \"{}\": {}", name, err)
                })
            })
            .unwrap_or(Ok($default))
    }};
    ($type:ty, $name:expr => $default:expr) => {
        get_env!($name, $type => $default)
    };
}
