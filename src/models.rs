use crate::schema::items;
use chrono::NaiveDateTime;

#[derive(Queryable, Debug)]
pub struct Item {
    pub id: i32,
    pub guid: String,
    pub read: bool,
    pub pub_date: Option<String>,
    pub title: Option<String>,
    pub link: Option<String>,
    pub comments_link: Option<String>,
    pub creators_json: Option<String>,
    pub description: Option<String>,
    pub creation_timestamp: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "items"]
pub struct NewItem<'a> {
    pub guid: &'a str,
    pub read: Option<bool>,
    pub pub_date: Option<&'a str>,
    pub title: Option<&'a str>,
    pub link: Option<&'a str>,
    pub comments_link: Option<&'a str>,
    pub creators_json: Option<String>,
    pub description: Option<&'a str>,
}

impl<'a> NewItem<'a> {
    pub fn from_rss_item(item: &rss::Item) -> NewItem {
        let guid = item.guid().unwrap().value();
        let pub_date = item.pub_date.as_ref().map(|d| d.as_str());
        let title = item.title.as_ref().map(|t| t.as_str());
        let link = item.link.as_ref().map(|l| l.as_str());
        let comments_link = item.comments.as_ref().map(|c| c.as_str());
        let description = item.description.as_ref().map(|d| d.as_str());
        let creators_json = item.dublin_core_ext().map(|d| {
            let c = &d.creators();
            let j = serde_json::to_string(c).unwrap();
            j
        });

        NewItem {
            guid,
            read: None,
            pub_date,
            title,
            link,
            comments_link,
            creators_json,
            description,
        }
    }
}
