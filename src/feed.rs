use std::error;

use reqwest::Response;
use rss::Channel;

pub async fn fetch_channel(url: &str) -> Result<Channel, Box<dyn error::Error>> {
    let response = fetch_url(url).await?;
    let body = get_response_body(response).await?;
    let channel = parse_channel(&body).await?;
    Ok(channel)
}

pub async fn parse_channel(body: &str) -> Result<Channel, rss::Error> {
    let channel = Channel::read_from(&mut body.as_bytes());
    channel
}

pub async fn fetch_url(url: &str) -> Result<Response, reqwest::Error> {
    let client = reqwest::Client::new();
    let response = client.get(url).send().await;
    response
}

pub async fn get_response_body(response: Response) -> Result<String, reqwest::Error> {
    let body = response.text().await;
    body
}
