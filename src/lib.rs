mod feed;
mod feed_reader;
mod get_env;
mod log_stream;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

pub mod db;
pub mod models;
pub mod schema;

use std::thread;

use feed_reader::FeedReader;
use log::{debug, error, info};
use rss::Item;
use teloxide::{
    prelude::{Requester, RequesterExt},
    types::ParseMode,
    Bot,
};

pub struct Config {
    pub telegram_api_url: reqwest::Url,
    pub telegram_token: String,
    pub telegram_chat_id: i64,
    pub feed_url: reqwest::Url,
    pub fetch_interval_sec: u64,
    pub db_url: String,
}

impl Config {
    pub fn from_env() -> Result<Config, Box<dyn std::error::Error>> {
        Ok(Config {
            telegram_api_url: get_env!("TELEGRAM_API_URL", reqwest::Url
                => reqwest::Url::parse(";https://api.telegram.org").unwrap())?,
            telegram_token: get_env!("TELEGRAM_TOKEN")?,
            telegram_chat_id: get_env!("TELEGRAM_CHAT_ID", i64)?,
            feed_url: get_env!("FEED_URL", reqwest::Url
                => reqwest::Url::parse("https://hnrss.org/best?points=250").unwrap())?,
            fetch_interval_sec: get_env!("FETCH_INTERVAL_SEC", u64 => 600)?,
            db_url: get_env!("DB_FILE" => {
                let cwd = std::env::current_dir().unwrap();
                cwd.join("hnrss.sqlite3").display()
            }),
        })
    }
}

pub async fn run(mut cfg: Config) -> ! {
    // Use new ORM db connection
    let conn = db::connect(&cfg.db_url);
    db::migrate(&conn);

    let mut reader = FeedReader::new(cfg.feed_url.as_str(), &conn);

    let telegram = Bot::new(cfg.telegram_token)
        .set_api_url(cfg.telegram_api_url)
        .parse_mode(ParseMode::Html)
        .auto_send();

    loop {
        info!("Fetching new items...");

        let items = match reader.fetch_new().await {
            Ok(items) => items,
            Err(err) => {
                if err.is::<reqwest::Error>() {
                    error!("Error fetching feed: {}", err);
                } else if err.is::<rss::Error>() {
                    error!("Error parsing feed: {}", err);
                } else {
                    error!("Error: {}", err);
                }

                error!("Retrying in {} seconds", cfg.fetch_interval_sec);
                thread::sleep(std::time::Duration::from_secs(cfg.fetch_interval_sec));
                continue;
            }
        };

        info!("Found {} new items", items.len());

        if items.len() > 0 {
            for item in items {
                info!(
                    "Sending item with guid: {}",
                    item.guid.as_ref().unwrap().value()
                );
                debug!("{:?}", &item);

                let mut success = false;
                while !success {
                    let result = telegram
                        .send_message(cfg.telegram_chat_id, &text_for_item(&item))
                        .await;
                    match result {
                        Ok(_) => success = true,
                        Err(err) => {
                            error!("Failed sending message: {}", err);
                            match err {
                                teloxide::RequestError::MigrateToChatId(id) => {
                                    cfg.telegram_chat_id = id;
                                    continue;
                                }
                                teloxide::RequestError::RetryAfter(sec) => {
                                    let sec =
                                        u64::try_from(sec).unwrap_or(cfg.fetch_interval_sec) + 5;
                                    error!("Retrying in {} seconds", sec);
                                    thread::sleep(std::time::Duration::from_secs(sec));
                                    continue;
                                }
                                teloxide::RequestError::Network(_) => {
                                    let sec = cfg.fetch_interval_sec;
                                    error!("Network error, retrying in {} seconds", sec);
                                    thread::sleep(std::time::Duration::from_secs(sec));
                                    continue;
                                }
                                teloxide::RequestError::InvalidJson { source, raw } => {
                                    error!("Invalid JSON: {}", source);
                                    error!("Raw JSON: {}", raw);
                                    error!("Skipping item {}", item.guid.as_ref().unwrap().value());
                                    break;
                                }
                                teloxide::RequestError::Io(_) => {
                                    error!(
                                        "I/O error, skipping item {}",
                                        item.guid.as_ref().unwrap().value()
                                    );
                                    break;
                                }
                                teloxide::RequestError::Api(api_err) => {
                                    error!("API Error: {}", api_err);
                                    error!("Skipping item {}", item.guid.as_ref().unwrap().value());
                                    break;
                                }
                            }
                        }
                    };
                }

                if success {
                    reader.mark_as_read(&item);
                }
            }

            info!("Done sending");
        }

        debug!("Next fetch in {} seconds", cfg.fetch_interval_sec);
        thread::sleep(std::time::Duration::from_secs(cfg.fetch_interval_sec));
    }
}

fn text_for_item(item: &Item) -> String {
    let title = item.title.as_ref().unwrap();
    let description = item
        .description
        .as_ref()
        .unwrap_or(&"".to_string())
        .replace("<p>", "\n")
        .replace("</p>", "")
        .replace("<hr>", "\n----------\n");

    format!("\n<b>{}</b>{}", title, description)
}
