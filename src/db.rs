use diesel::prelude::*;
use diesel_migrations::{any_pending_migrations, embed_migrations};
use embedded_migrations::run_with_output;
use log::info;

use crate::log_stream::LogStream;

embed_migrations!();

pub fn connect(db_url: &str) -> SqliteConnection {
    SqliteConnection::establish(&db_url).expect(&format!("Error connecting to {}", db_url))
}

pub fn migrate(conn: &SqliteConnection) {
    let pending = any_pending_migrations(conn).expect("Error checking for pending migrations");
    if !pending {
        return;
    }

    info!("Migrating DB");
    run_with_output(conn, &mut LogStream::new(|line| info!("{}", line)))
        .expect("Error running migrations");
    info!("Migration complete");
}
