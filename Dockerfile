FROM rust as build

# Use the same name as in Cargo.toml
# Don't forget to change after the next `FROM`
ARG APP_NAME=hnrss-bot

# Create a new empty shell project
RUN USER=root cargo init --bin --name "${APP_NAME}" "/app"
WORKDIR "/app"

# Copy manifests and cache dependencies
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
RUN set -xeu ;\
    cargo build --release ;\
    rm src/*.rs

RUN ls /app

# Copy source
COPY ./src ./src
COPY ./migrations ./migrations

# Build for release
RUN set -xeu ;\
    rm -f "./target/release/deps/${APP_NAME}"* ;\
    touch ./src/main.rs ;\
    cargo build --release



# The actual container
FROM debian:bullseye-slim

# Use the same name as in Cargo.toml
ARG APP_NAME=hnrss-bot

# Install dependencies
RUN set -xeu ;\
    apt-get update ;\
    apt-get install -y --no-install-recommends \
        ca-certificates \
        openssl \
        sqlite3 \
    ;\
    rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app

# Copy the build artifact from the build stage
COPY --from=build "/app/target/release/${APP_NAME}" .

RUN set -xeu ;\
    echo >>/entrypoint.sh "#!/usr/bin/env bash" ;\
    echo >>/entrypoint.sh "exec \"/app/${APP_NAME}\" \"\$@\"" ;\
    chmod +x /entrypoint.sh

# Set the startup command to run the app
ENTRYPOINT ["/entrypoint.sh"]
