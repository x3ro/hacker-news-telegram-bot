use std::process;

use dotenv::dotenv;
use hnrss_bot::*;
use log::{error, info};

#[tokio::main]
async fn main() {
    dotenv().ok();
    pretty_env_logger::init_timed();

    let cfg = Config::from_env().unwrap_or_else(|err| {
        error!("Problem getting config from environment: {}", err);
        process::exit(1);
    });

    info!("FEED_URL: {}", cfg.feed_url);
    info!("DB_FILE: {}", cfg.db_url);
    info!("FETCH_INTERVAL_SEC: {}", cfg.fetch_interval_sec);

    info!("Starting hnrss-bot");

    run(cfg).await;
}
