use std::error;

use diesel::prelude::*;
use log::debug;
use rss::Item;

use crate::feed::fetch_channel;

pub struct FeedReader<'a> {
    url: String,
    conn: &'a SqliteConnection,
}

impl<'a> FeedReader<'a> {
    pub fn new(url: &str, conn: &'a SqliteConnection) -> FeedReader<'a> {
        FeedReader {
            url: url.to_string(),
            conn,
        }
    }

    pub async fn fetch_new(&mut self) -> Result<Vec<Item>, Box<dyn error::Error>> {
        let channel = fetch_channel(&self.url).await?;

        let mut new_items: Vec<Item> = Vec::new();

        for item in channel.items {
            let guid = get_guid(&item);

            if self.is_read(&guid) {
                continue;
            }

            debug!("New guid: {}", guid);
            self.insert_item_to_db(&item);

            new_items.push(item);
        }

        Ok(new_items)
    }

    pub fn mark_as_read(&mut self, item: &rss::Item) {
        use crate::schema::items::dsl::*;

        let updated = diesel::update(items.filter(guid.eq(get_guid(item))))
            .set(read.eq(true))
            .execute(self.conn)
            .expect("Error updating item");
        if updated != 1 {
            panic!("Failed to update item");
        }
    }

    fn is_read(&mut self, guid: &str) -> bool {
        use crate::schema::items;

        let count: i64 = items::table
            .filter(items::guid.eq(guid))
            .count()
            .get_result(self.conn)
            .unwrap();
        count > 0
    }

    fn insert_item_to_db(&self, item: &Item) {
        use crate::models::NewItem;
        use crate::schema::items;

        let new_item = NewItem::from_rss_item(item);
        diesel::insert_into(items::table)
            .values(new_item)
            .execute(self.conn)
            .unwrap();
    }
}

fn get_guid(item: &Item) -> &str {
    item.guid().unwrap().value()
}
