table! {
    items (id) {
        id -> Integer,
        guid -> Text,
        read -> Bool,
        pub_date -> Nullable<Timestamp>,
        title -> Nullable<Text>,
        link -> Nullable<Text>,
        comments_link -> Nullable<Text>,
        creators_json -> Nullable<Text>,
        description -> Nullable<Text>,
        creation_timestamp -> Timestamp,
    }
}
