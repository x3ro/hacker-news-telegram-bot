-- Create databas if it does not exits
CREATE TABLE IF NOT EXISTS `items` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    `guid` TEXT UNIQUE NOT NULL,
    `read` BOOLEAN NOT NULL DEFAULT 0,
    `pub_date` DATETIME,
    `title` TEXT,
    `link` TEXT,
    `comments_link` TEXT,
    `creators_json` TEXT,
    `description` TEXT,
    `creation_timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
-- Copy table to make sure any existing table will have the correct schema
CREATE TABLE `items___tmp` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    `guid` TEXT UNIQUE NOT NULL,
    `read` BOOLEAN NOT NULL DEFAULT 0,
    `pub_date` DATETIME,
    `title` TEXT,
    `link` TEXT,
    `comments_link` TEXT,
    `creators_json` TEXT,
    `description` TEXT,
    `creation_timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO items___tmp (
        `id`,
        `guid`,
        `read`,
        `pub_date`,
        `title`,
        `link`,
        `comments_link`,
        `creators_json`,
        `description`,
        `creation_timestamp`
    )
SELECT `id`,
    `guid`,
    `read`,
    `pub_date`,
    `title`,
    `link`,
    `comments_link`,
    `creators_json`,
    `description`,
    `creation_timestamp`
from items;
-- Drop old table and rename copy to old table
DROP TABLE items;
ALTER TABLE items___tmp
    RENAME TO items;