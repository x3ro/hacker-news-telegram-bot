let
  rust-overlay = (import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/4770c6c6cb518d2c5b660c5099816c3512d8bdad.tar.gz"));

  # Pinned nixpkgs, deterministic. Last updated: 2022-04-16T11:26:18Z [nixos-unstable].
  pkgs = import (fetchTarball(
    "https://github.com/NixOS/nixpkgs/archive/bc4b9eef3ce3d5a90d8693e8367c9cbfc9fc1e13.tar.gz")) {
      overlays = [ rust-overlay ];
    };

  # Specific NixOS release version. This is not the channel and does not get updated along with it.
  #pkgs = import (fetchTarball("https://github.com/NixOS/nixpkgs/archive/refs/tags/21.11.tar.gz")) {};

  # Rolling updates, not deterministic.
  #pkgs = import (fetchTarball("channel:nixpkgs-unstable")) {};

  rust-nightly = pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default);
  # Wrap only `cargo-expand`.
  cargo-expand = pkgs.writeShellScriptBin "cargo-expand" ''
    export RUSTC="${rust-nightly}/bin/rustc";
    export CARGO="${rust-nightly}/bin/cargo";
    exec "${pkgs.cargo-expand}/bin/cargo-expand" "$@"
  '';
in pkgs.mkShell {
  name = "hn-rss";

  packages = with pkgs; [
    bashInteractive
    (rust-bin.stable.latest.default.override {
      extensions = ["rust-src"];
    })
    cargo-expand
    pkg-config
    openssl.dev
    sqlite
    diesel-cli
  ];
}
